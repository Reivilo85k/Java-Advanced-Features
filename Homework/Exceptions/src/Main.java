import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.next();

        if (isInteger(input)){
        }else if (isDouble(input)){
        }else {
            System.out.println("Hey ! That's not a value ! Try once more");
        }
    }

    public static boolean isInteger(String input) {
        try {
            int x = Integer.parseInt(input);
            System.out.println("int -> " + input);
            return true;
        } catch (NumberFormatException e) { }
        return false;
    }

    public static boolean isDouble(String input) {
        try {
            double x = Double.parseDouble(input);
            System.out.println("double -> " + input);
            return true;
        }catch (NumberFormatException f){}
        return false;
    }
}