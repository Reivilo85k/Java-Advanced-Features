import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        List<String> employees = new ArrayList<>();
        employees.add("Margus");
        employees.add("Olivier");
        employees.add("Minez");
        employees.add("Irma");

        EmployeeChecker margus = new EmployeeChecker(employees.get(0));
        margus.start();
        Thread.sleep(2000 - margus.employeeLeavingCounter);
        EmployeeChecker olivier = new EmployeeChecker(employees.get(1));
        olivier.start();
        Thread.sleep(1000 - olivier.employeeLeavingCounter);
        EmployeeChecker minez = new EmployeeChecker(employees.get(2));
        minez.start();
        Thread.sleep(3000 -  minez.employeeLeavingCounter);
        EmployeeChecker irma = new EmployeeChecker(employees.get(3));
        irma.start();
    }
}