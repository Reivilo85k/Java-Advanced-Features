// You are the manager. You have 5 employees. Simulate the situation in which each of
// them comes at a different time to work.
// a) Every employee, after getting to work, displays the information „„<name: I came
// to work at <time HH:MM>."
// b) Every 10 seconds, the employee displays "name: I'm still working!"
// c) Every 30 seconds, we release one of the employees to home (remember about
// stopping the thread!) and remove the employee from the active employees list.
// d) When you release your employee to home, print „<name: <time HH:MM>, it's// time to go home!"
// e) * When you release a given employee, all of the others speed up. From that
// moment, display the information about work 2 seconds faster// f) The manager decides in which order release employees (e.g. through an
// earlier defined list).

import java.time.LocalTime;

public class EmployeeChecker extends Thread {

    private String employee;
    private final LocalTime localTime = LocalTime.now();
    private boolean isStillWorking = true;
    private int threadSleepTime = 10000;
    int interval = 1;
    long employeeLeavingCounter = 0;

    EmployeeChecker(String nameOfEmployee) {
        this.employee = nameOfEmployee;
    }

    public void setStillWorking(boolean stillWorking) {
        isStillWorking = stillWorking;
    }

    public void setThreadSleepTime(int threadSleepTime) {
        this.threadSleepTime = threadSleepTime;
    }

    @Override
    public void run() {
        printTheEmployeeStatus();

        try {
            showTheEmployeeStatusEvery10Sec();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void showTheEmployeeStatusEvery10Sec() throws InterruptedException {
        Thread.sleep(threadSleepTime);
        printTheEmployeeStatus();

        if (interval < 4) {
            interval++;
            showTheEmployeeStatusEvery10Sec();
        } else {
            employeeExit();
        }
    }

    private void employeeExit() {
        employeeLeavingCounter -= 2000;
        System.out.println(employee + " Went home");
    }

    private void printTheEmployeeStatus() {
        System.out.println(employee + " Came at " + localTime.toString());
    }
}
