package Animals;

public class Main {

    public static void main(String[] args) {
        Animal[] pets = {new Cat("Fluffy", "Siamese", 2 ), new Dog("Rantanplan", "Labrador", 4)};

        // final method
        for(Animal element : pets){
            element.yieldVoice(element);
        }
        // additional final method
        for(Animal element : pets){
            System.out.println(element.toString());

        }
    }
}
