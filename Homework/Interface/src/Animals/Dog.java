package Animals;

public class Dog extends Animal{


    public Dog(String name, String race, Integer age) {
        super(name, "Dog", age);
    }

    @Override
    public void yieldVoice(Animal animal) {
        System.out.println(animal.getType() + " : Ouah-Ouah");
    }
}
