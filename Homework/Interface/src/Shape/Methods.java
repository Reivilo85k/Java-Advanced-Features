package Shape;

public interface Methods {
    public abstract double getArea();
    public abstract double getPerimeter();
}
