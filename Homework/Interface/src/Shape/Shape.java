package Shape;

public abstract class Shape implements Methods {
    private Integer numberOfSides;
    private double area;
    private double perimeter;

    public Shape(Integer numberOfSides) {
        this(numberOfSides, 0, 0);
    }

    public Shape(Integer numberOfSides, double area, double perimeter) {
        this.numberOfSides = numberOfSides;
        this.area = area;
        this.perimeter = perimeter;
    }
    public Integer getNumberOfSides() {
        return numberOfSides;
    }
    public void setNumberOfSides(Integer numberOfSides) {
        this.numberOfSides = numberOfSides;
    }
    public void setArea(double area) {
        this.area = area;
    }
    public void setPerimeter(double perimeter) {
        this.perimeter = perimeter;
    }
}
