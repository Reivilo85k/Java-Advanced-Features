package Shape;

public class Rectangle extends Shape {
    private double a;
    private double b;

    public Rectangle(double a, double b) {
        super(4);
        setA(a);
        setB(b);
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double calculateArea() {
        double area = a*b;
        setArea(area);
        return area;
    }

    public double calculatePerimeter(){
        double perimeter = 2*a+2*b;
        setPerimeter(perimeter);
        return perimeter;
    }

    @Override
    public double getArea() {
        return calculateArea();
    }

    @Override
    public double getPerimeter() {
        return calculatePerimeter();
    }

}
