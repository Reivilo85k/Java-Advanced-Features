package Shape;

public class Circle extends Shape{
    private double radius;

    public Circle(double radius) {
        super(0);
        setRadius(radius);
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double calculateArea() {
        double area = (Math.PI*(radius*radius));
        setArea(area);
        return area;
    }

    public double calculatePerimeter(){
        double perimeter = 2*Math.PI*radius;
        setPerimeter(perimeter);
        return perimeter;
    }

    @Override
    public double getArea() {
        return calculateArea();
    }

    @Override
    public double getPerimeter() {
        return calculatePerimeter();
    }
}
