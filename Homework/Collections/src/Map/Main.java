package Map;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Map<String, String> names = new HashMap<>();
        names.put("Laborde", "Olivier");
        names.put("Bros", "Mario");
        names.put("Odinson", "Thor");
        names.put("of Rivia", "Geralt");
        names.put("Drake", "Nathan");

        for (Map.Entry<String, String> registry : names.entrySet()) {
            String name = registry.getKey();
            String surname = registry.getValue();
            System.out.printf("%s : %s\n", name, surname);
        }

        Map<String, Integer> ages = new HashMap<>();
        ages.put("Olivier", 39);
        ages.put("Mario", 35);
        ages.put("Thor", 4500);
        ages.put("Geralt", 165);
        ages.put("Nathan", 34);

        for (Map.Entry<String, Integer> registry2 : ages.entrySet()) {
            String name = registry2.getKey();
            Integer age = registry2.getValue();
            System.out.printf("%s : %d\n", name, age);
        }
    }
}
