package List;

import java.util.Scanner;

public interface Methods {
    void addElement(Object item);
    void removeElement(Object item);
    boolean checkItemInCart(Item item);
    void purchases(Scanner scanner);
    void deletePurchase(Scanner scanner);
    void displayElementsStartingWithM();
    void displayElementsNextToElementsStartingWithM();
    void displayFullCart();
}
