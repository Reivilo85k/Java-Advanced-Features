
package List.Multiplications;

        import java.util.ArrayList;
        import java.util.List;

public class Tables2 {
    public static void main(String[] args) {
        ArrayList<ArrayList<Integer>> tables = new ArrayList<ArrayList<Integer>>();
        ArrayList<Integer> factors = new ArrayList<Integer>();
        for (int i = 0; i < 10; i++) {
            factors.add(i + 1);
        }

        for (int i = 0; i < 10; i++) {
            for (int index = 0; index < 10; index++) {
                factors.set(index, (index + 1) * (i + 1));
            }
            tables.add(i,factors);
        }

        for (int i = 0; i < 10; i++) {
            System.out.println(tables.get(i) + "\n");
        }
    }
}
