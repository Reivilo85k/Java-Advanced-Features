package List;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Cart<Item> cart = new Cart();
        shoppingSession(cart);

        System.out.println("Thank you for your purchase !");

    }

    public static void shoppingSession (Cart cart){
        Scanner scanner = new Scanner(System.in);
        System.out.printf("A NEW LIST HAS BEEN CREATED\n");
        for (int i = 0; i < 1; ) {
            System.out.printf("\nPlease chose an option :\n" +
                    "1. Make a purchase\n" +
                    "2. Delete an element from the list\n" +
                    "3. Display purchases starting with m\n" +
                    "4. Display purchases whose next to product starting with m\n" +
                    "5. Display whole list\n" +
                    "6. Checkout\n" +
                    "input : ");
            int choice = scanner.nextInt();

            switch (choice) {
                case (1):
                    cart.purchases(scanner);
                    break;
                case (2):
                    cart.deletePurchase(scanner);
                    break;
                case (3):
                    cart.displayElementsStartingWithM();
                    break;
                case (4):
                    cart.displayElementsNextToElementsStartingWithM();
                    break;
                case (5):
                    cart.displayFullCart();
                    break;
                case (6):
                    i++;
                    break;
                default:
                    System.out.println("Input error please try again");
            }
        }
    }
}
