package List;

import java.util.ArrayList;
import java.util.Scanner;

public class Cart<T extends Item> implements Methods {
   private ArrayList<T> cart = new ArrayList<T>();


    @Override
    public void addElement(Object item) {
        cart.add((T) item);
    }

    @Override
    public void removeElement(Object item) {
        for (T el : cart) {
            if (el.getClass() == item.getClass()) {
                cart.remove(el);
            }
        }
    }

    @Override
    public void purchases(Scanner scanner) {
        for (int i = 0; i < 1; ) {
            Item item = choseItem(scanner);
            if (item.getName().equals("I/O error")) {
                System.out.println("Input error");
            }else if (cart == null) {
                addElement(item);
            } else if (!checkItemInCart(item)) {
                addElement(item);
            } else if (checkItemInCart(item)) {
                System.out.println("Item already in cart");
            }
            System.out.println("Press F to add another element or M to go back to Menu");
            Object input2 = scanner.next();
            if (!input2.equals("F")) {
                i++;
            }
        }
    }

    @Override
    public boolean checkItemInCart(Item item){
        for (T el : cart){
            if (el.getClass() == item.getClass()){
                return true;
            }
        }return false;
    }

    @Override
    public void deletePurchase(Scanner scanner){
        for (int i = 0; i < 1; ) {
            Item item = choseItem(scanner);
            if (item.getName().equals("I/O error")) {
                System.out.println("Input error");
            }else if (cart == null) {
                System.out.println("Nothing to delete");
            } else if (checkItemInCart(item)) {
                removeElement(item);
            } else if (!checkItemInCart(item)) {
                System.out.println("Item not in cart");
            }
            System.out.println("Press F to delete another element or M to go back to Menu");
            Object input2 = scanner.next();
            if (!input2.equals("F")) {
                i++;
            }
        }
    }

    @Override
    public void displayElementsStartingWithM() {
        System.out.println("PRODUCTS STARTING WITH M IN YOUR CART : \n");
        for (Item item : cart) {
            if (item.getName().startsWith("M")) {
                System.out.println(item.getName() + "");
            }
        }
    }

    @Override
    public void displayElementsNextToElementsStartingWithM() {
        System.out.println("\nPRODUCTS NEXT TO PRODUCTS STARTING WITH M IN YOUR CART : \n");
        for (int i = cart.size()-1; i >= 0; i--) {
            if (cart.get(i).getName().startsWith("M")) {
                if (!cart.get(i-1).getName().startsWith("M")){
                    if (i != 0) {
                        System.out.println(cart.get(i - 1).getName() + "");
                    }
                }
            }
        }
    }


    @Override
    public void displayFullCart() {
        System.out.println("YOUR CART : \n");
        for (Item item : cart){
            System.out.println(item.getName() + "");
        }
    }
    public static Item choseItem(Scanner scanner) {
        Item item;
        String input = "";
        System.out.printf("\nPlease chose an item :\n" +
                "1. Eggs\n" +
                "2. Mackerel\n" +
                "3. Milk\n" +
                "4. Mineral water\n" +
                "5. Pork\n" +
                "6. Potatoes\n" +
                "7. Yogurt\n");
        input = scanner.next();
        switch (input) {
            case ("1"):
                item = new Eggs();
                return item;
            case ("2"):
                item = new Mackerel();
                return item;
            case ("3"):
                item = new Milk();
                return item;
            case ("4"):
                item = new MineralWater();
                return item;
            case ("5"):
                item = new Pork();
                return item;
            case ("6"):
                item = new Potatoes();
                return item;
            case ("7"):
                item = new Yogurt();
                return item;
            default:
                item = new NullItem();
                return item;
        }
    }
}
