package List.Ratings;

import java.util.ArrayList;
import java.util.Random;

public class Ratings {
    public static void main(String[] args) {
        Random rand = new Random();
        ArrayList<Integer> ratings = new ArrayList<Integer>();

        for (int i = 0; i < 25; i++) {
            ratings.add(i, rand.nextInt(6) + 1);
        }

        System.out.println(ratings + "");

        double temp = 0;
        for (int index : ratings) {
            temp += index;
        }
        double average = temp/(ratings.size());
        System.out.printf("The average is %.2f",average);
    }
}
