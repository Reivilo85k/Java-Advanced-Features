package Set;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Main {
    public static void main(String[] args) {

        Set<String> colors = new HashSet<>();
        colors.add("Blue");
        colors.add("Yellow");
        colors.add("Red");
        colors.add("Brown");
        colors.add("Green");
        colors.add("Pink");

       System.out.println(colors);

        String remove = "Pink";
        colors.removeIf(remove::equals);
        System.out.println(colors);
    }
}
