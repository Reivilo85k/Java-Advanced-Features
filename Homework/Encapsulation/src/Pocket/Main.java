package Pocket;

public class Main {

    public static void main(String[] args) {
        Pocket wallet = new Pocket(300);

        wallet.setMoney(5);
        System.out.println(wallet.toString());

        wallet.setMoney(4000);
        System.out.println(wallet.toString());

        wallet.setMoney(1000);
        System.out.println(wallet.toString());

    }
}
