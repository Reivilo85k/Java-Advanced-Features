package Pocket;

public class Pocket {
    private double money;

    public Pocket(double money) {
        this.money = money;
    }

    public double getMoney() {
        if (money <= 10){
            return 0;
        }else {
            return money;
        }
    }

    public void setMoney(double money) {
        if (money >= 0 && money <= 3000){
            this.money = money;
        } else if (money > 3000){
            System.out.println("I don't have enough space in my pocket for as much money");
        }else if (money < 0){
            System.out.println("You cannot add minus values you thief");
        }
    }

    @Override
    public String toString() {
        return "Pocket{" +
                "money=" + getMoney() +
                '}';
    }
}
