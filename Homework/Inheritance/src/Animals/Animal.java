package Animals;

public class Animal {
    private String name;
    private String type;
    private String talk;
    private Integer age;

    public Animal(String name, String type, String talk, Integer age) {
        this.name = name;
        this.type = type;
        this.talk = talk;
        this.age = age;
    }

    public String getName() {
        if (name != null) {
            return name;
        }return "name unknown";
    }

    public void setName(String name) {
        if (name != null) {
            this.name = name;
        }
    }

    public String getType() {
        if (type != null) {
            return type;
        }return "race unknown";
    }

    public void setType(String race) {
        if (race != null) {
            this.type = race;
        }
    }

    public String getTalk() {
        if (talk != null) {
            return talk;
        }return "undefined noise";
    }

    public void setTalk(String talk) {
        if (talk != null) {
            this.talk = talk;
        }
    }

    public Integer getAge() {
        if(age >=0) {
            return age;
        }return 0;
    }

    public void setAge(Integer age) {
        if(age >=0) {
            this.age = age;
        }
    }
    public static void yieldVoice(Animal animal){
        System.out.println(animal.getType() + " : " + animal.getTalk());
    }

    @Override
    public String toString() {
        return "Animal{" +
                "name='" +getName() + '\'' +
                ", type='" + getType() + '\'' +
                ", talk='" + getTalk() + '\'' +
                ", age=" + getAge() +
                '}';
    }
}
