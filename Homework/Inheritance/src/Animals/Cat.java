package Animals;

public class Cat extends Animal{

    public Cat(String name, String race, Integer age) {
        super(name, "Cat", "Miaou", age);
    }

}
