package Shape;

public class Rectangle extends Shape {
    private double a;
    private double b;

    public Rectangle(Integer numberOfSides, double area, double perimeter, double a, double b) {
        super(4, area = a*b, perimeter = 2*a+2*b);
    }
}
