package Shape;

public class Circle extends Shape{
    private double radius;

    public Circle(Integer numberOfSides, double area, double perimeter, double radius) {
        super(0, area = (Math.PI*(radius*radius)), perimeter = 2*Math.PI*radius );
    }
}
