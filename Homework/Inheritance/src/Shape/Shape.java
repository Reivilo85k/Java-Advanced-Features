package Shape;

public class Shape {
    private Integer numberOfSides;
    private double area;
    private double perimeter;

    public Shape(Integer numberOfSides, double area, double perimeter) {
        this.numberOfSides = numberOfSides;
        this.area = area;
        this.perimeter = perimeter;
    }

    public Integer getNumberOfSides() {
        return numberOfSides;
    }

    public void setNumberOfSides(Integer numberOfSides) {
        this.numberOfSides = numberOfSides;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getPerimeter() {
        return perimeter;
    }

    public void setPerimeter(double perimeter) {
        this.perimeter = perimeter;
    }
}
