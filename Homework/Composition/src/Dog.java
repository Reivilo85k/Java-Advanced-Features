public class Dog {
    private String name;
    private Muzzle muzzle;


    public Dog(String name, Muzzle muzzle) {
        this.name = name;
        this.muzzle = muzzle;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Muzzle getMuzzle() {
        return muzzle;
    }

    public void setMuzzle(Muzzle muzzle) {
        this.muzzle = muzzle;
    }

    @Override
    public String toString() {
        return "Dog{" +
                "name='" + getName() + '\'' +
                ", muzzle material='" + getMuzzle().getMaterial() + '\'' +
                ", muzzle color=" + getMuzzle().getColor() +

                '}';
    }
}
