public class Main {

    public static void main(String[] args) {
        Muzzle muzzle1 = new Muzzle("leather", "brown");
        Dog DoggyMcDogFace = new Dog("DoggyMcDogFace", muzzle1);

        System.out.println(DoggyMcDogFace.toString());
    }
}
