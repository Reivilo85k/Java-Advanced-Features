
public enum Planets {
    MERCURY("Mercury",0.0553, "small Mercury", 0.61),
    VENUS("Venus",0.815, "Almost standard Venus", 0.28),
    EARTH("Earth",1, "Average Earth", 0),
    MARS("Mars",0.107, "Small and mighty Mars", 0.52),
    JUPITER("Jupiter",317.8, "Humongous Jupiter", 4.2),
    SATURN("Saturn", 97.2, "SuperGiant Saturn", 8.52),
    URANUS("Uranus", 14.5, "Uranus is quite big", 18.21),
    NEPTUNE("Neptune", 17.1, "Giant Neptune", 29.09),
    PLUTO("Pluto",0.0025, "Tiny Pluto", 39);

    String name;
    double mass;
    String relativeString;
    double distanceFromEarth;

    Planets(String name, double mass, String relativeString, double distanceFromEarth) {
        this.name = name;
        this.mass = mass;
        this.relativeString = relativeString;
    }

     public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMass() {
        return mass;
    }

    public void setMass(double mass) {
        this.mass = mass;
    }

    public String getRelativeString() {
        return relativeString;
    }

    public void setRelativeString(String relativeString) {
        this.relativeString = relativeString;
    }

    public double getDistanceFromEarth() {
        return distanceFromEarth;
    }

    public void setDistanceFromEarth(double distanceFromEarth) {
        this.distanceFromEarth = distanceFromEarth;
    }

    @Override
    public String toString() {
        return "Planets{" +
                "This planet is named " + name + " It is "+ distanceFromEarth + " AU from Earth" +
                ", its mass is " + mass + " times the mass of our planet " +
                "and that is why we can say '" + relativeString + '\'' +
                '}';
    }
}


