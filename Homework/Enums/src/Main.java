import java.util.Random;

public class Main {

    public static Planets randomPlanet(Planets planet) {
        Random randomNum = new Random();
        final int result = randomNum.nextInt(8);

        if (result == 0) {
            return Planets.MERCURY;
        } else if (result == 1) {
            return Planets.VENUS;
        } else if (result == 2) {
            return Planets.EARTH;
        } else if (result == 3) {
            return Planets.MARS;
        } else if (result == 4) {
            return Planets.JUPITER;
        } else if (result == 5) {
            return Planets.SATURN;
        } else if (result == 6) {
            return Planets.URANUS;
        } else if (result == 7) {
            return Planets.NEPTUNE;
        } else if (result == 0) {
            return Planets.PLUTO;
        } else {
            return Planets.EARTH;
        }
    }

    public static void main(String[] args) {
        Planets planet = Planets.EARTH;
        planet = randomPlanet(planet);

        System.out.println(planet);
    }
}
