public class GTMain {

    public static void main(String[] args) {
        String string = "This is a string";
        Integer integer = 42;
        Double doubleVar = 9.99;

        GenericBox genericBox = new GenericBox();
        genericBox.addGenericBoxElement(string);
        genericBox.addGenericBoxElement(integer);
        genericBox.addGenericBoxElement(doubleVar);

        for (Object element : genericBox.getGenericArray()){
            System.out.println(element + " ");
        }


    }
}
