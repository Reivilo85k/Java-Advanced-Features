import java.util.ArrayList;

public class GenericBox <T>{
 private ArrayList<T> genericArray = new ArrayList<>();

    public GenericBox() {
    }

    public ArrayList<T> getGenericArray() {
        return genericArray;
    }

    public void setGenericBox(ArrayList<T> genericArray) {
        this.genericArray = genericArray;
    }

    public void addGenericBoxElement(T item){
        this.genericArray.add(item);
    }
}
