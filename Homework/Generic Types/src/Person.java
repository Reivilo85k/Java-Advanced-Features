import java.math.BigDecimal;

public class Person implements Comparable<Person>{
    private double size;

    public Person(double size) {
        this.size = size;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    @Override
    public double compareTo(Person otherPerson) {
        return this.size - otherPerson.size;
    }
}
