public interface Comparable<T> {

 double compareTo(T o);
}
