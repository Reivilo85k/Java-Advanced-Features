package Animals;

public abstract class Animal {
    private String name;
    private String type;
    private Integer age;

    public Animal(String name, String type, Integer age) {
        this.name = name;
        this.type = type;
        this.age = age;
    }

    public String getName() {
        if (name != null) {
            return name;
        }return "name unknown";
    }

    public void setName(String name) {
        if (name != null) {
            this.name = name;
        }
    }

    public String getType() {
        if (type != null) {
            return type;
        }return "race unknown";
    }

    public void setType(String race) {
        if (race != null) {
            this.type = race;
        }
    }

    public Integer getAge() {
        if(age >=0) {
            return age;
        }return 0;
    }

    public void setAge(Integer age) {
        if(age >=0) {
            this.age = age;
        }
    }
    public abstract void yieldVoice(Animal animal);


    @Override
    public String toString() {
        return "Animal{" +
                "name='" +getName() + '\'' +
                ", type='" + getType() + '\'' +
                ", age=" + getAge() +
                '}';
    }
}
