package Animals;

public class Cat extends Animal{

    public Cat(String name, String race, Integer age) {
        super(name, "Cat", age);
    }

    @Override
    public void yieldVoice(Animal animal) {
        System.out.println(animal.getType() + " : Miaou");
    }
}
