package Shape;

import java.util.GregorianCalendar;

import static java.lang.Math.round;

public class Main {

    public static void main(String[] args) {
        Circle circle = new Circle(3);
        Rectangle rectangle = new Rectangle(4,5);

        System.out.println("The circle radius being : " + circle.getRadius() + " cm. long");
        System.out.println("its area is : " + round(circle.getArea()) + " cm2. wide");
        System.out.println("and its circle perimeter is : " + round(circle.getPerimeter()) + " cm. long");
        System.out.println("The rectangle side A is "
                + rectangle.getA()
                + " cm. long and its side B is "
                + rectangle.getB()
                + " cm. long therefore : ");
        System.out.println("its rectangle area is : " + rectangle.getArea() + " cm. long");
        System.out.println("and its rectangle perimeter is : " + rectangle.getPerimeter()+ " cm. long");

    }
}
